===== Midnight dharma web site with rss-feed

Full webpage with update script or offline downloader. 

.
|-- downloader.py             # script for download all mp3 from server
|-- feed_gen.py               # preconfigured rss feed generator
|-- requirements.txt          # python requirements 
|-- rss_feed.j2               # rss feed template
`-- www                       # site source visible from browser 
    |-- index.html
    |-- ipodcast.png
    |-- pulnocni-dharma.jpg
    `-- rss                   # url for rss feed
        `-- index.xml         # generated rss feed by update script


==== Maintanance

Site include two silly scripts. Both have configuration in script, so before first run don't forgot to review capitalized variables!!!


=== Update rss feed

Generate rss feed to path www/rss/index.xml. Script runs for a while becouse must get informations about all mp3.

./feed_gen.py www/rss/index.xml



=== Download mp3 files

Just download mp3 files for offline usage. 

./downloader.py


